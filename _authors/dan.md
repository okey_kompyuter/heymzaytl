---
short_name: dan
name_yi: דניאל נעמעני
name_en: Daniel Nemenyi
role:
photo: dan.jpg
twitter: danielnemenyi
biog_yi: לערנט אינעם דיגיטאַל־הומאַניסטיק אָפּטײל פֿון קינגס קאָלעדזש לאָנדאָן
biog_en: Teaches in the <a href="https://www.kcl.ac.uk/people/daniel-nemenyi">Department of Digital Humanities, King's College London</a>
---

