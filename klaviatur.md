---
layout: page
title: קלאַװיאַטור
title_en: Keyboard
permalink: /klaviatur/
---

<h2 class="en">Introduction</h2>

<p class="en">The Okey_Kompyuter Yiddish keyboard layout offers the most intuitive and comprehensive way to type in Standard Yiddish ('YIVO Rules'). Tweaked at for over eight years and supporting Windows 10 as well as macOS (Linux to come), we hope you'll like it!</p>

<p class="en">The keyboard continues the discontinued <a href="https://web.archive.org/web/20180325015625/http://shretl.org/">shretl.org</a> keyboard project, taking some inspiration from <a href="https://www.isaacbleaman.com/resources/yiddish_typing/">Isaac L. Bleaman's layout</a> and making some key changes (pun intended).</p>

<p class="en">It supports currency symbols relevant to the Yiddish world ($, ¢ and £ but also ₪, zł and ¥), accents to mark word emphases and allows the user to switch between Yiddish and English with the caps lock key.</p>

<p class="en">Because it implements a one-key-one-character and one-character-one-key policy, a couple of Yiddish-specific characters may display awkwardly in fonts which don't explicitly implement Yiddish characters. For example יִ displays as <span style="font-family: Arial;">יִ</span> in Arial, with its misplaced pintl. We believe that the time has come to fully support Yiddish Unicode and fix broken fonts, rather than distribute one-key-<i>two</i>-character layouts again, which function like bicycle stabilisers - a short-term fix at the detriment of long-term growth. We have no plans to distribute layouts which are compatable with fonts which do not support Yiddish: it's time Hebrew fonts became compatible with Yiddish.</p>

<p class="en">For now, we advise users to employ Yiddish-friendly fonts like <a href="https://fonts.google.com/specimen/David+Libre" title="Download from Google Fonts">David Libre</a> wherever feasable.</p>

<p class="en">We will endeavor to help if you are having problems, though please note the installation of this software comes entirely at your own responsibility and we cannot accept any liability should problems arise.</p>

<p class="en">These keyboard layout is offered under a <a href="https://www.gnu.org/licenses/gpl-3.0.en.html">GPL-3 Licence</a>, so feel free to distribute and  <a href="https://gitlab.com/okey_kompyuter/yiddish-klaviator">fork it</a> as you please.</p>

<h2 class="en">Installation</h2>

<h3 class="en">MacOS</h3>

<p class="en"><i>Note you will need to be logged in an admin user, and using a Macbook or iMac. This will not work on iPhones or iPads. </i></p>

<ol class="en">
<li>Download and save the <a href="https://gitlab.com/okey_kompyuter/yiddish-klaviator/-/raw/master/Ok-Kompyuter-Yiddish-Keyboard-2.0.dmg">ok_k-yiddish-klaviatur.dmg</a> installer.</li>
<li>Mount it (double click on the downloaded file and wait for a pop up to appear), drag and drop the ok_k-yiddish-klaviatur.bundle (with the yod) into the Keyboard Layouts folder.
<ul>
<li>If you're being prevented from dragging and droping, right click on ok_k-yiddish-klaviatur.bundle, click Copy (or hit cmd-c), then right click on Keyboard Layouts and click Paste Item (or cmd-v).</li>
</ul>
</li>
<li>Enter your user password when asked.</li>
<li> Open up your Keyboard Prefences by pressing the Apple icon () in your navbar, select 'System Preferences...', 'Keyboard' and then the 'Input Sources' tab.
     <ul>
     <li>If you're already using multiple layouts you can just click on your current layout in the navbar and select 'Open Keyboard Preferences' from the drop-down menu.</li>
     </ul>
</li>
<li>In Keyboard Preferences, click the + icon in the bottom-left. Search for Yiddish, and then 'Yiddish - QWERTY'. Click on it and then Add. That's it, it's installed. Now...</li>
<li>To switch to the Yiddish keyboard, in the taskbar (top of the screen), next to the date and time, you should see a flag. Click it and then 'Yiddish - QWERTY'. Select and you're done! Open TextEdit or Word and get typing.</li>
</ol>

<p class="en">You can view an interactive map of the keys by clicking on the golden Yod in the taskbar and then 'Show Keyboard Viewer'. Otherwise you may wish to download and print our <a href="{{ "/zakhn/ok_k-map-macos-yiddish-qwerty.pdf" | prepend: site.baseurl }}">map of the keys</a>.</p>

<h3 class="en">Windows 10</h3>

<ol class="en">
<li>Download and save the <a href="https://gitlab.com/okey_kompyuter/yiddish-klaviator/-/raw/master/ok_k-yiddish-qwerty.exe?inline=false">ok_k-yiddish-qwerty.exe</a> Windows installer.</li>

<li>Double click on it. 'Do you want to allow this app...?' Yes. Install.</li>

<li>Hit the Start button. Locate 'Settings'.</li>

<li>Enter 'Time and Language'. In the sidebar you should see 'Language'. Select 'Add a Language'. Search for Yiddish, then click Next and then Install.</li>

<li>You should now see ייִדיש listed. Click on it, then 'Options', 'Add a Keyboard' and locate 'Yiddish QWERTY'.</li>

<li>Now you can switch to your new Yiddish keyboard. In the bottom-right of your screen, near to the time and date, you should see an abreviation of your current language (ENG for English). Click on it, and if all went well you should now see ייִדיש too. Click it and you're done!</li>

</ol>

<p class="en">You may wish to download and print our <a href="{{ "/zakhn/ok_k-map-windows-yiddish-qwerty-map.pdf" | prepend: site.baseurl }}">map of the keys</a>.</p>

<p class="en">We also recommend you install the excellent Yiddish-friendly font <a href="https://fonts.google.com/specimen/David+Libre" title="Download from Google Fonts">David Libre</a>, which you are currently reading.</p>
