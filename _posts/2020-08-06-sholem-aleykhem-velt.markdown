---
layout: post
title:  "שלום־עליכם װעלט!"
date:   2020-08-04 15:32:14 -0300
categories: jekyll update
author: דניאל נעמעני
lang: yi
---

<p class="en">Hello World!</p>

<p class="en">So what's this whole okey_kompyuter thing about, you ask?</p>

<p class="en">The belief that Yiddish needs new tech to grow into the future. That to start with, it needs some pretty basic tech indeed. Up to now there has been still no simple, cross-platform, built-in way to type Yiddish letters. There is still no decent spell checker. No social media platform based in Standard Yiddish orthography. Too few fonts which support all Yiddish Unicode characters properly (the wonderful David Libre being the noble  exception). There's little in the way of documentation: How do you run a properly bidirectional blog in Yiddish and English? How do you typeset Yiddish in LaTeX? How do you make new Yiddish fonts, or even just install and use the ones that exist?</p>

<p class="en">Okey_kompyuter is an initiative to start a <a href="https://en.wikipedia.org/wiki/Free_and_open-source_software">Free and Open Source</a> community to build these things. It's not just for coders and programmers. We need testers, writers, designers, gamers, artists, publishers, musicians, translators... In short, Yiddishist community-organisers of all stripes. If you're interested in any of this then please get involved!</p>

<p class="en">The project is being launched with a <a href="{{ "/klaviatur" | prepend: site.baseurl }}">new QWERTYish Yiddish keyboard layout</a> for and MacOS and Windows 10. It's been tweaked at for over eight years and we hope you like it.</p>

<p class="en">If you'd like to participate in this project then please join our <a href="https://groups.google.com/forum/#!forum/okey_kompyuter">Google Group</a> and check out our <a href="https://gitlab.com/okey_kompyuter">gitlab repositories</a>. If that's too much for you then at least say gut morgen <a href="https://twitter.com/okey_kompyuter">on Twitter</a>! We look forward to meeting you.</p>

<p>דניאל נעמעני / Daniel Nemenyi<br>August 2020</p>